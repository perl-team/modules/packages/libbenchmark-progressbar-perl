Source: libbenchmark-progressbar-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Niko Tyni <ntyni@debian.org>
Section: perl
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13), libmodule-install-perl
Build-Depends-Indep: libterm-progressbar-perl,
                     perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libbenchmark-progressbar-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libbenchmark-progressbar-perl.git
Homepage: https://metacpan.org/release/Benchmark-ProgressBar

Package: libbenchmark-progressbar-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libterm-progressbar-perl
Description: Perl module that displays progress bar during benchmarking
 Benchmark::ProgressBar is a combination of Benchmark and Term::ProgressBar
 that produces a simple progress bar illustrating execution of benchmark
 runs. As such, it is particularly useful for heavy benchmarking tests
 that take a long time to complete.
 .
 You can use it as a drop-in replacement for Benchmark, but the only functions
 that would display a progress bar are: cmpthese, timethese and timeit.
 .
 This module is not compatible for use with Benchmark in the same script. The
 author doesn't know of any case where this would make sense.
